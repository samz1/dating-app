import {
  MemberMessagesComponent
} from './members/member-messages/member-messages.component';
import {
  MessgaesResolver
} from './_resolvers/messages.resolver';
import {
  BrowserModule
} from '@angular/platform-browser';
import {
  NgModule
} from '@angular/core';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  HttpClientModule
} from '@angular/common/http';

import {
  BsDropdownModule,
  BsDatepickerModule,
  TabsModule,
  PaginationModule,
  ButtonsModule
} from 'ngx-bootstrap';
import {
  JwtModule
} from '@auth0/angular-jwt';
import {
  NgxGalleryModule
} from 'ngx-gallery';
import {
  FileUploadModule
} from 'ng2-file-upload';
import {
  TimeAgoPipe
} from 'time-ago-pipe';


import {
  AppRoutingModule
} from './app-routing.module';
import {
  AppComponent
} from './app.component';
import {
  NavComponent
} from './nav/nav.component';
import {
  HomeComponent
} from './home/home.component';
import {
  RegisterComponent
} from './register/register.component';
import {
  ErrorInterceptorProvider
} from './_services/error.interceptor.ts.service';
import {
  AlertifyService
} from './_services/alertify.service';

import {
  MemberListComponent
} from './members/member-list/member-list.component';
import {
  ListsComponent
} from './lists/lists.component';
import {
  MessagesComponent
} from './messages/messages.component';

import {
  MemberCardComponent
} from './members/member-card/member-card.component';
import {
  UserService
} from './_services/user.service';
import {
  AuthGuard
} from './_guards/auth.guard';
import {
  AuthService
} from './_services/auth.service';
import {
  MemberDetailComponent
} from './members/member-detail/member-detail.component';
import {
  MemberDetailResolver
} from './_resolvers/member-detail.resolver';
import {
  MembersListResolver
} from './_resolvers/members-list.resolver';
import {
  MemberEditComponent
} from './members/member-edit/member-edit.component';
import {
  MemberEditResolver
} from './_resolvers/member-edit.resolver';
import {
  PreventUnsavedChanges
} from './_guards/prevent-unsaved-changes.guard';
import {
  PhotoEditorComponent
} from './members/photo-editor/photo-editor.component';
import {
  ListsResolver
} from './_resolvers/lists.resolver';


export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    RegisterComponent,
    MemberListComponent,
    ListsComponent,
    MessagesComponent,
    MemberCardComponent,
    MemberDetailComponent,
    MemberEditComponent,
    PhotoEditorComponent,
    TimeAgoPipe,
    MemberMessagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    NgxGalleryModule,
    PaginationModule.forRoot(),
    FileUploadModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:5000'],
        blacklistedRoutes: ['localhost:5000/api/auth']
      }
    })
  ],
  providers: [
    AuthService,
    ErrorInterceptorProvider,
    AlertifyService,
    AuthGuard,
    PreventUnsavedChanges,
    UserService,
    MemberDetailResolver,
    MembersListResolver,
    MemberEditResolver,
    ListsResolver,
    MessgaesResolver
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
