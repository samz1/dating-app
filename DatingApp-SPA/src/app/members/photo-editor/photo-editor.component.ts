import {
  AlertifyService
} from './../../_services/alertify.service';
import {
  UserService
} from './../../_services/user.service';
import {
  AuthService
} from './../../_services/auth.service';
import {
  Photo
} from './../../_models/Photo';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import {
  FileUploader
} from 'ng2-file-upload';
import {
  environment
} from '../../../environments/environment';


@Component({
  selector: 'app-photo-editor',
  templateUrl: './photo-editor.component.html',
  styleUrls: ['./photo-editor.component.css']
})
export class PhotoEditorComponent implements OnInit {

  @Input() photos: Photo[];
  @Output() getMemberPhotoChange = new EventEmitter < string > ();
  private baseUrl = environment.apiUrl;
  currentMain: Photo;

  uploader: FileUploader;
  hasBaseDropZoneOver = false;

  constructor(private authService: AuthService, private userService: UserService, private alertify: AlertifyService) {}

  ngOnInit() {
    this.initializeUploader();
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  initializeUploader() {
    this.uploader = new FileUploader({
      url: this.baseUrl + 'users/' + this.authService.decodedToken.nameid + '/photos',
      authToken: 'Bearer ' + localStorage.getItem('token'),
      isHTML5: true,
      allowedFileType: ['image'],
      removeAfterUpload: true,
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024 // Max File size 10 Megabytes
    });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (response) {
        const res: Photo = JSON.parse(response);
        const photo: Photo = {
          id: res.id,
          url: res.url,
          dateAdded: res.dateAdded,
          description: res.description,
          isMain: res.isMain
        };
        this.photos.push(photo);
        if (photo.isMain) {
          this.authService.changeMemberPhoto(photo.url);
          this.authService.currentUser.photoUrl = photo.url;
          localStorage.setItem('user', JSON.stringify(this.authService.currentUser));
        }
      }
    };
  }


  setmainPhoto(photo: Photo) {
    this.userService.setMainPhoto(this.authService.decodedToken.nameid, photo.id)
      .subscribe(
        () => {
          this.currentMain = this.photos.filter(p => p.isMain === true)[0];
          this.currentMain.isMain = false;
          photo.isMain = true;
          this.authService.changeMemberPhoto(photo.url);
          this.authService.currentUser.photoUrl = photo.url;
          localStorage.setItem('user', JSON.stringify(this.authService.currentUser));
        },
        error => {
          this.alertify.error(error);
          console.log('Error Setting Photo to Main:\n');
          console.log(error);
        }
      );
  }

  deletePhoto(photoId: number) {

    this.alertify.confirm('Warning!', 'Are you sure you want to delete this photo.',
      () => {
        this.userService.deletePhoto(this.authService.decodedToken.nameid, photoId)
          .subscribe(
            () => {
              this.photos.splice(this.photos.findIndex(p => p.id === photoId), 1);
              this.alertify.success('Photo has been deleted');
            },
            error => {
              this.alertify.error(error);
            }
          );
      }
    );
  }


}
