import {
  User
} from './../_models/user';
import {
  AuthService
} from './../_services/auth.service';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import {
  AlertifyService
} from '../_services/alertify.service';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';
import {
  BsDatepickerConfig
} from 'ngx-bootstrap';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public user: User;
  public registrationFrom: FormGroup;
  public bsConfig: Partial < BsDatepickerConfig > ;

  @Output() cancelRegister = new EventEmitter();


  constructor(
    private authService: AuthService,
    private alertify: AlertifyService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {

    this.bsConfig = {
      containerClass: 'theme-red'
    };

    this.createRegisterForm();
  }


  passwordMathValidator(g: FormGroup) {
    return g.get('password').value === g.get('confirmPassword').value ? null : {
      'mismatch': true
    };
  }

  createRegisterForm() {
    this.registrationFrom = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(16)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100)]],
      // Additional Fields
      gender: ['male'],
      knownAs: ['', Validators.required],
      dateOfBirth: [null, Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required]
    }, {
      validator: this.passwordMathValidator
    });

  }

  register() {

    if (this.registrationFrom.valid) {
      this.user = Object.assign({}, this.registrationFrom.value);

      this.authService.register(this.user).subscribe(
        () => {
          console.log('Registration Successful');
          this.alertify.success('Registration Successful');
        },
        error => {
          console.log('Registration Failed!\n ' + error);
          this.alertify.error(error);
        },
        () => {
          this.authService.login(this.user)
            .subscribe(
              () => {
                this.router.navigate(['/members']);
              }
            );
        }
      );

    }
    console.log(this.registrationFrom.value);
  }


  cancel() {
    this.cancelRegister.emit(false);
    console.log('Registration Cancelled');
  }

}
