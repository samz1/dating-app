import {
  MessgaesResolver
} from './_resolvers/messages.resolver';
import {
  PreventUnsavedChanges
} from './_guards/prevent-unsaved-changes.guard';
import {
  MemberEditResolver
} from './_resolvers/member-edit.resolver';
import {
  MemberEditComponent
} from './members/member-edit/member-edit.component';
import {
  MemberDetailResolver
} from './_resolvers/member-detail.resolver';
import {
  MemberDetailComponent
} from './members/member-detail/member-detail.component';
import {
  AuthGuard
} from './_guards/auth.guard';
import {
  ListsComponent
} from './lists/lists.component';
import {
  HomeComponent
} from './home/home.component';
import {
  NgModule
} from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {
  MemberListComponent
} from './members/member-list/member-list.component';
import {
  MessagesComponent
} from './messages/messages.component';
import {
  MembersListResolver
} from './_resolvers/members-list.resolver';
import {
  ListsResolver
} from './_resolvers/lists.resolver';



const routes: Routes = [{
    path: '',
    component: HomeComponent
  },
  {
    path: '', // You can use 'admin' here to have routes like '/admin/members' ....
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [{
        path: 'members',
        component: MemberListComponent,
        resolve: {
          users: MembersListResolver
        }
      },
      {
        path: 'members/:id',
        component: MemberDetailComponent,
        resolve: {
          user: MemberDetailResolver
        }
      },
      {
        path: 'member/edit',
        component: MemberEditComponent,
        resolve: {
          user: MemberEditResolver
        },
        canDeactivate: [PreventUnsavedChanges]
      },
      {
        path: 'messages',
        component: MessagesComponent,
        resolve: {
          messages: MessgaesResolver
        }
      },
      {
        path: 'lists',
        component: ListsComponent,
        resolve: {
          users: ListsResolver
        }
      },
    ]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  },
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
