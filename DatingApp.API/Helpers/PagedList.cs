using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Helpers
{
    public class PagedList<T> : List<T>
    {
        public PagedList(List<T> items, int itemCount, int pageNumber, int pageSize)
        {
            this.CurrentPage = pageNumber;

            this.PageSize = pageSize;
            this.TotalCount = itemCount;

            if (pageSize > 0)
            {
                this.TotalPages = (int)Math.Ceiling(itemCount / (double)pageSize);
            }

            this.AddRange(items);
        }

        public static async Task<PagedList<T>> CreateAsync(IQueryable<T> source, int pageNumber, int pageSize)
        {
            // Get total count of items
            var count = await source.CountAsync();
            // Get items according to paging information
            var items = await source.Skip(((pageNumber - 1) * pageSize)).Take(pageSize).ToListAsync();

            return new PagedList<T>(items, count, pageNumber, pageSize);
        }

        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }


    }
}