using System.Linq;
using DatingApp.API.DTOs;
using DatingApp.API.Models;

namespace DatingApp.API.Helpers
{
    public class AutoMapperProfiles : AutoMapper.Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>()
                .ForMember(destinaiton => destinaiton.PhotoUrl, option =>
                {
                    option.MapFrom(source => source.Photos.FirstOrDefault(p => p.IsMain).Url);
                })
                .ForMember(destinaiton => destinaiton.Age, option =>
                {
                    option.MapFrom(source => source.DateOfBirth.CalculateAge());
                });

            CreateMap<User, UserForDetailedDto>()
                .ForMember(destinaiton => destinaiton.PhotoUrl, option =>
                {
                    option.MapFrom(source => source.Photos.FirstOrDefault(p => p.IsMain).Url);
                })
                .ForMember(destinaiton => destinaiton.Age, option =>
                {
                    option.MapFrom(source => source.DateOfBirth.CalculateAge());
                });

            CreateMap<UserForUpdateDto, User>();
            CreateMap<Photo, PhotoForDetailedDto>();
            CreateMap<Photo, PhotoToReturnDto>();
            CreateMap<PhotoForCreationDto, Photo>();
            CreateMap<UserForRegisterDto, User>();

            CreateMap<MessageForCreationDto, Message>().ReverseMap();
            CreateMap<Message, MessageToReturnDto>()
                .ForMember(x => x.SenderPhotoUrl, option => option.MapFrom(x => x.Sender.Photos.FirstOrDefault(p => p.IsMain).Url))
                .ForMember(x => x.RecipientPhotoUrl, option => option.MapFrom(x => x.Recipient.Photos.FirstOrDefault(p => p.IsMain).Url));

        }
    }
}