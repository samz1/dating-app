using System.ComponentModel.DataAnnotations;
using System;

namespace DatingApp.API.DTOs
{
    public class UserForRegisterDto
    {
        [Required]
        [StringLength(16, MinimumLength = 3, ErrorMessage = "You must specify {0} between {2} and {1} characters")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 8, ErrorMessage = "You must specify {0} between {2} and {1} characters")]
        public string Password { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public string KnownAs { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Country { get; set; }

        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }

        public UserForRegisterDto()
        {
            Created = DateTime.Now;
            LastActive = DateTime.Now;
        }
    }
}