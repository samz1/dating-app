using System;
using System.Threading.Tasks;
using DatingApp.API.Contracts;
using DatingApp.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Data
{
    public class AuthRepository : IAuthRepository
    {
        // Private Propterties
        // ===================================================================
        private readonly DataContext context;


        // Constructor
        // ===================================================================
        public AuthRepository(DataContext contextParam)
        {
            this.context = contextParam;

        }

        // Login Method : IAuthRepository
        // ===================================================================
        public async Task<User> LoginAsync(string username, string password)
        {
            var user = await context.Users.Include(p => p.Photos).FirstOrDefaultAsync(x => x.Username.Equals(username));

            if (user == null)
            {
                return null;
            }
            else if (!VerifyPassword(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }
            else
            {
                return user;
            }
        }




        // Register Method : IAuthRepository
        // ===================================================================
        public async Task<User> RegisterAsync(User user, string password)
        {
            byte[] passwordHash, passwordSalt;
            this.CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordSalt = passwordSalt;
            user.PasswordHash = passwordHash;

            await context.Users.AddAsync(user);
            await context.SaveChangesAsync();

            return user;
        }


        // User Exists Method : IAuthRepository
        // ===================================================================
        public async Task<bool> UserExistsAsync(string username)
        {
            if (await context.Users.AnyAsync(x => x.Username.Equals(username)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        // Create Password Hash Method
        // ===================================================================
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }


        // Verify Password Method
        // ===================================================================
        private bool VerifyPassword(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}