using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Contracts;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Data
{
    public class DatingRepository : IDatingRepository
    {
        // Private Propterties
        // ===================================================================
        private readonly DataContext context;


        // Constructor
        // ===================================================================
        public DatingRepository(DataContext contextParam)
        {
            this.context = contextParam;
        }

        // Get Like
        // ===================================================================
        public async Task<Like> GetLike(int userId, int recipientId)
        {
            return await context.Likes.FirstOrDefaultAsync(u => u.LikerId == userId && u.LikeeId == recipientId);
        }

        // Add User & Photo
        // ===================================================================
        public void Add<T>(T entity) where T : class
        {
            context.Add(entity);
        }

        // Add User & Photo Asynchronously
        // ===================================================================
        public async void AddAsync<T>(T entity) where T : class
        {
            await context.AddAsync(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            context.Remove(entity);
        }

        public async Task<Photo> GetMainPhotoForUser(int userId)
        {
            return await context.Photos.Where(u => u.UserId == userId).FirstOrDefaultAsync(p => p.IsMain);
        }

        // Get Photo
        // ===================================================================
        public async Task<Photo> GetPhoto(int id)
        {
            return await context.Photos.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<User> GetUserAsync(int id)
        {
            return await context.Users.Include(p => p.Photos).FirstOrDefaultAsync(u => u.Id == id);
        }

        // Get a User Asynchronously
        // ===================================================================
        public async Task<PagedList<User>> GetUsersAsync(UserParams userParams)
        {
            var users = context.Users.Include(p => p.Photos).OrderByDescending(u => u.LastActive).AsQueryable();

            // Filters 
            users = users.Where(u => u.Id != userParams.UserId);
            users = users.Where(u => u.Gender.Equals(userParams.Gender));

            // Get the list of users that have liked the user
            if (userParams.Likers)
            {
                var userLikers = await GetUserLikesAsync(userParams.UserId, userParams.Likers);
                users = users.Where(u => userLikers.Contains(u.Id));
            }

            // Get the list of users that user have liked
            if (userParams.Likees)
            {
                var userLikees = await GetUserLikesAsync(userParams.UserId, userParams.Likers);

                // TODO: Fix it is returning only one user back
                users = users.Where(u => userLikees.Contains(u.Id));
            }

            // Check if there are Min and Max Age defined
            if (userParams.MinAge != 18 || userParams.MaxAge != 99)
            {
                var minDateOfBirth = DateTime.Today.AddYears(-userParams.MaxAge - 1);
                var maxDateOfBirth = DateTime.Today.AddYears(-userParams.MinAge);
                users = users.Where(u => u.DateOfBirth >= minDateOfBirth && u.DateOfBirth <= maxDateOfBirth);
            }

            if (!string.IsNullOrEmpty(userParams.OrderBy))
            {
                switch (userParams.OrderBy)
                {
                    case "created":
                        users = users.OrderByDescending(u => u.Created);
                        break;
                    default:
                        users = users.OrderByDescending(u => u.LastActive);
                        break;
                }
            }

            return await PagedList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
        }

        // Get Id's of Likers and Likees of a User Asynchronously
        // ===================================================================
        private async Task<IEnumerable<int>> GetUserLikesAsync(int id, bool likers)
        {
            var user = await context.Users
            .Include(x => x.Likers)
            .Include(x => x.Likees)
            .FirstOrDefaultAsync(u => u.Id == id);

            // Get all the ids of users that liked our currently logged in user
            if (likers == true)
            {
                return user.Likers.Where(u => u.LikeeId == id).Select(i => i.LikerId);
            }
            // Get ids of users that our currently logged in users liked
            else
            {
                return user.Likees.Where(u => u.LikerId == id).Select(i => i.LikeeId);
            }
        }

        public async Task<bool> SaveAllAsync()
        {
            return await context.SaveChangesAsync() > 0;
        }

        // Get a message
        // ===================================================================
        public async Task<Message> GetMessage(int id)
        {
            return await context.Messages.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<PagedList<Message>> GetMessgesForUser(MessageParams messageParams)
        {
            var messages = context.Messages
                                    .Include(x => x.Sender)
                                    .ThenInclude(x => x.Photos)
                                    .Include(x => x.Recipient)
                                    .ThenInclude(x => x.Photos)
                                    .AsQueryable();

            switch (messageParams.MessageContainer)
            {
                case "Inbox":
                    messages = messages.Where(x => x.RecipientId == messageParams.UserId && x.RecipientDeleted == false);
                    break;
                case "Outbox":
                    messages = messages.Where(x => x.SenderId == messageParams.UserId && x.SenderDeleted == false);
                    break;
                default:
                    messages = messages.Where(x => x.RecipientId == messageParams.UserId && x.RecipientDeleted == false && x.IsRead == false);
                    break;
            }

            messages = messages.OrderByDescending(x => x.MessageSent);
            return await PagedList<Message>.CreateAsync(messages, messageParams.PageNumber, messageParams.PageSize);

        }

        public async Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId)
        {
            var messages = await context.Messages
                                     .Include(x => x.Sender)
                                     .ThenInclude(x => x.Photos)
                                     .Include(x => x.Recipient)
                                     .ThenInclude(x => x.Photos)
                                     .Where(x => x.RecipientId == userId && x.RecipientDeleted == false && x.SenderId == recipientId
                                     || x.RecipientId == recipientId && x.SenderId == userId && x.RecipientDeleted == false)
                                     .OrderByDescending(x => x.MessageSent).ToListAsync();
            return messages;

        }
    }
}