using System.Collections.Generic;
using System.Threading.Tasks;
using DatingApp.API.Helpers;
using DatingApp.API.Models;

namespace DatingApp.API.Contracts
{
    public interface IDatingRepository
    {
        // Generics are used to avoid having multiple functions for Users & Photos
        void Add<T>(T entity) where T : class;
        void AddAsync<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAllAsync();
        Task<PagedList<User>> GetUsersAsync(UserParams userParams);
        Task<User> GetUserAsync(int id);
        Task<Photo> GetPhoto(int id);
        Task<Photo> GetMainPhotoForUser(int userId);
        Task<Like> GetLike(int userId, int recipientId);

        // Get a single message from database
        Task<Message> GetMessage(int id);

        // Ge a list of message for user
        Task<PagedList<Message>> GetMessgesForUser(MessageParams messageParams);

        Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId);

    }
}