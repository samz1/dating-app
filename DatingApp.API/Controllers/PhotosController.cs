using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using DatingApp.API.Data;
using DatingApp.API.DTOs;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using DatingApp.API.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace DatingApp.API.Controllers
{
    [Route("api/users/{userId}/photos")]
    [ApiController]
    [Authorize]
    public class PhotosController : ControllerBase
    {
        // Private Properties
        // ===================================================================
        private readonly IDatingRepository repo;
        private readonly IOptions<CloudinarySettings> cloudniaryConfig;
        private readonly Cloudinary cloudinary;
        private IMapper mapper;

        // Constructor
        // ===================================================================
        public PhotosController(IDatingRepository repoInjection, IOptions<CloudinarySettings> cloudinaryConfig, IMapper autoMapper)
        {
            this.mapper = autoMapper;
            this.cloudniaryConfig = cloudinaryConfig;
            this.repo = repoInjection;

            Account cloudniaryAccount = new Account(
                cloudniaryConfig.Value.CloudName,
                cloudniaryConfig.Value.ApiKey,
                cloudniaryConfig.Value.ApiSecret
            );

            cloudinary = new Cloudinary(cloudniaryAccount);
        }

        // Set a Photo as Main
        // ===================================================================
        [HttpPost("{id}/setMain")]
        public async Task<IActionResult> SetMainPhoto(int userId, int id)
        {
            // Check for valid token user
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            // Check if the user has that photo
            var userFromRepo = await repo.GetUserAsync(userId);

            if (!userFromRepo.Photos.Any(p => p.Id == id))
            {
                return Unauthorized();
            }

            // Get the Photo to set to main.
            var photoFromRepo = await repo.GetPhoto(id);

            // See if it's already main.
            if (photoFromRepo.IsMain)
            {
                return BadRequest("This is already the main photo.");
            }

            // Get the current Main Photo and set it to false.
            var currentMainPhoto = await repo.GetMainPhotoForUser(userId);
            currentMainPhoto.IsMain = false;

            // Set requested photo to main.
            photoFromRepo.IsMain = true;

            // Save all changes
            if (await repo.SaveAllAsync())
            {
                return NoContent();
            }
            else
            {
                return BadRequest("Couln't set photo to main.");
            }

        }

        // Get Photo
        // ===================================================================
        [HttpGet("{id}", Name = "GetPhoto")]
        public async Task<IActionResult> GetPhoto(int id)
        {
            var photoFromRepo = await repo.GetPhoto(id);
            var photoToReturn = mapper.Map<PhotoToReturnDto>(photoFromRepo);

            return Ok(photoToReturn);
        }


        // Set a Photo as Main
        // ===================================================================
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhoto(int userId, int id)
        {
            // Check for valid token user
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            // Check if the user has that photo
            var userFromRepo = await repo.GetUserAsync(userId);

            if (!userFromRepo.Photos.Any(p => p.Id == id))
            {
                return Unauthorized();
            }

            // Get the Photo to set to main.
            var photoFromRepo = await repo.GetPhoto(id);

            // See if it's already main.
            if (photoFromRepo.IsMain)
            {
                return BadRequest("You cannot delete your main photo.");
            }


            // You'll need to delete photo on Cloudinary and Its reference in SQLite DB

            if (photoFromRepo.PublicId != null)
            {
                var deleteParams = new DeletionParams(photoFromRepo.PublicId);
                var response = cloudinary.Destroy(deleteParams);

                if (response.Result.Equals("ok"))
                {
                    repo.Delete(photoFromRepo);
                }
            }
            else
            {
                repo.Delete(photoFromRepo);
            }


            if (await repo.SaveAllAsync())
            {
                return Ok();
            }
            else
            {
                return BadRequest("Failed to delete Photo");
            }
        }

        // Add Photo for User with specified Id
        // ===================================================================
        [HttpPost]
        public async Task<IActionResult> AddPhotoForUser(int userId, [FromForm] PhotoForCreationDto photoForCreation)
        {
            // Check for valid token user
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var userFromRepo = await repo.GetUserAsync(userId);

            var file = photoForCreation.File;

            // Store result for Cloudinary after upload
            var uploadResult = new ImageUploadResult();

            if (file.Length > 0)
            {
                using (var stream = file.OpenReadStream())
                {
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(file.Name, stream),
                        Transformation = new Transformation().Width(500).Height(500).Crop("fill").Gravity("face")
                    };

                    uploadResult = cloudinary.Upload(uploadParams);
                }
            }

            photoForCreation.Url = uploadResult.Uri.ToString();
            photoForCreation.PublicId = uploadResult.PublicId;

            var photo = mapper.Map<Photo>(photoForCreation);

            if (!userFromRepo.Photos.Any(u => u.IsMain))
            {
                photo.IsMain = true;
            }

            userFromRepo.Photos.Add(photo);



            if (await repo.SaveAllAsync())
            {
                // If save is successful, then the PHOTO will have the id
                var photoToReturn = mapper.Map<PhotoToReturnDto>(photo);

                return CreatedAtRoute("GetPhoto", new { id = photo.Id }, photoToReturn);
            }
            else
            {
                return BadRequest("Couldn't add the photo.");
            }
        }
    }
}