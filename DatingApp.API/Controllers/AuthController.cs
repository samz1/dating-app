using System.Threading.Tasks;
using AutoMapper;
using DatingApp.API.Contracts;
using DatingApp.API.DTOs;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DatingApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        // Private Properties
        // ===================================================================
        private readonly IAuthRepository repo;

        private readonly IConfiguration config;

        private readonly IMapper mapper;

        // Constructor
        // ===================================================================
        public AuthController(IAuthRepository repoInjection, IConfiguration configInjection, IMapper mapper)
        {
            this.config = configInjection;
            this.repo = repoInjection;
            this.mapper = mapper;
        }


        // Register Controller
        // ===================================================================
        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegisterDto userForRegister)
        {
            // Validate Request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Do not accept duplicate usernames
            userForRegister.Username = userForRegister.Username.ToLower();

            // Check whether this username exits
            if (await repo.UserExistsAsync(userForRegister.Username))
            {
                return BadRequest("Username already exists.");
            }

            // Create a User object
            var userToCreate = mapper.Map<User>(userForRegister);

            // Create User using repository
            User createdUser = await repo.RegisterAsync(userToCreate, userForRegister.Password);
			
			var userToReturn = mapper.Map<UserForDetailedDto>(createdUser);

            return CreatedAtRoute("GetUser", new { Controller = "Users", Id = createdUser.Id}, userToReturn);
        }


        // Login Controller
        // ===================================================================
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLogin)
        {
            // throw new System.Exception("Computer Says No!");
            // Get user from database
            var userFromRepo = await repo.LoginAsync(userForLogin.Username.ToLower(), userForLogin.Password);

            // If user not found => return Unauthorized
            if (userFromRepo == null)
            {
                return Unauthorized();
            }


            // ========== JSON Web Token (JWT) ==========

            // 1. Token has claims
            var claims = new[]
            {
                new System.Security.Claims.Claim(System.Security.Claims.ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                new System.Security.Claims.Claim(System.Security.Claims.ClaimTypes.Name,userFromRepo.Username)
            };

            // 2. To validate token
            var key = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(config.GetSection("AppSettings:Token").Value));


            // 3. Encrypt the key with a hasing function
            var creds = new Microsoft.IdentityModel.Tokens.SigningCredentials(key, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha512Signature);

            //  4. Creating a token object
            var tokenDescriptor = new Microsoft.IdentityModel.Tokens.SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(claims),
                Expires = System.DateTime.Now.AddDays(90),          // For Testing only: it's a hassle to login each time you need to test the API.
                SigningCredentials = creds
            };

            // 5. Create a JWT
            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            var user = mapper.Map<UserForListDto>(userFromRepo);

            return Ok(new
            {
                token = tokenHandler.WriteToken(token),
                user
            });
        }

    }
}