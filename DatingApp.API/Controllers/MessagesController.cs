using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.API.Contracts;
using DatingApp.API.Data;
using DatingApp.API.DTOs;
using DatingApp.API.Models;
using DatingApp.API.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;


namespace DatingApp.API.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Route("api/users/{userId}/[controller]")]
    [ApiController]
    [Authorize]
    public class MessagesController : ControllerBase
    {

        // Private Properties
        // ===================================================================
        private readonly IDatingRepository repo;
        private readonly IMapper mapper;

        // Constructor
        // ===================================================================
        public MessagesController(IDatingRepository repoInjection, IMapper autoMapper)
        {
            this.mapper = autoMapper;
            this.repo = repoInjection;
        }


        // Get message thread for currently logged in user
        // ===================================================================
        [HttpGet("thread/{recipientId}")]
        public async Task<IActionResult> GetMessageThread(int userId, int recipientId)
        {
            // Check if user is authorized
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var messagesFromRepo = await repo.GetMessageThread(userId, recipientId);

            var messageThread = mapper.Map<IEnumerable<MessageToReturnDto>>(messagesFromRepo);

            return Ok(messageThread);
        }

        // Get a list of messages for currently logged in user
        // ===================================================================
        // Note! made a stupid mistake: Made the method private and server just kept
        // returing 405 method not allowed.
        [HttpGet]
        public async Task<IActionResult> GetMessagesForUser(int userId,
            [FromQuery] MessageParams messageParams)
        {
            // Check if user is authorized
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            messageParams.UserId = userId;

            var messagesFromRepo = await repo.GetMessgesForUser(messageParams);

            var messages = mapper.Map<IEnumerable<MessageToReturnDto>>(messagesFromRepo);

            Response.AddPaginationHeaders(messagesFromRepo.CurrentPage, messagesFromRepo.PageSize, messagesFromRepo.TotalCount, messagesFromRepo.TotalPages);

            return Ok(messages);
        }



        // Get a message for currently logged in user.
        // ===================================================================
        [HttpGet("{id}", Name = "GetMessage")]
        public async Task<IActionResult> GetMessage(int userId, int id)
        {
            // Check for valid token user
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var messageFromRepo = await repo.GetMessage(id);

            if (messageFromRepo == null)
            {
                return NotFound();
            }

            return Ok(messageFromRepo);
        }




        // Create a message
        // ===================================================================
        [HttpPost]
        public async Task<IActionResult> CreateMessage(int userId, MessageForCreationDto messageForCreation)
        {
            var sender = await repo.GetUserAsync(userId);

            // Check for valid token user
            if (sender.Id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            messageForCreation.SenderId = userId;

            var recipient = await repo.GetUserAsync(messageForCreation.RecipientId);

            if (recipient == null)
            {
                return BadRequest("Could not find user");
            }

            var message = mapper.Map<Message>(messageForCreation);

            repo.AddAsync(message);



            if (await repo.SaveAllAsync())
            {
                var messageToReturn = mapper.Map<MessageToReturnDto>(message);

                return CreatedAtRoute("GetMessage", new
                {
                    id = message.Id
                }, messageToReturn);
            }

            throw new Exception("Creating the message faild on save");
        }




        // Delete a message for currently logged in user
        // ===================================================================
        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteMessage(int id, int userId)
        {
            // Check if user is authorized
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var messageFromRepo = await repo.GetMessage(id);

            if (messageFromRepo.SenderId == userId)
            {
                messageFromRepo.SenderDeleted = true;
            }

            if (messageFromRepo.RecipientId == userId)
            {
                messageFromRepo.RecipientDeleted = true;
            }

            if (messageFromRepo.SenderDeleted && messageFromRepo.RecipientDeleted)
            {
                repo.Delete(messageFromRepo);
            }

            if (await repo.SaveAllAsync())
            {
                return NoContent();
            }

            throw new Exception("Error deleteing the messsage.");
        }


        // Mark message as read
        // ===================================================================
        [HttpPost("{id}/read")]
        public async Task<IActionResult> MarkMessageAsRead(int userId, int id)
        {
            // Check if user is authorized
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var messageFromRepo = await repo.GetMessage(id);

            if (messageFromRepo.RecipientId != userId)
            {
                return Unauthorized();
            }

            messageFromRepo.IsRead = true;
            messageFromRepo.DateRead = DateTime.Now;

            await repo.SaveAllAsync();
            return NoContent();
        }
    }



}