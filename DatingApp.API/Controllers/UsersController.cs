using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.API.Contracts;
using DatingApp.API.Data;
using DatingApp.API.DTOs;
using DatingApp.API.Models;
using DatingApp.API.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DatingApp.API.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        // Private Properties
        // ===================================================================
        private readonly IDatingRepository repo;
        private readonly IConfiguration config;
        public IMapper Mapper { get; set; }

        // Constructor
        // ===================================================================
        public UsersController(IDatingRepository repoInjection, IConfiguration configInjection, IMapper autoMapper)
        {
            this.Mapper = autoMapper;
            this.config = configInjection;
            this.repo = repoInjection;
        }

        // Get User's Like
        // ===================================================================
        [HttpPost("{id}/like/{recipientId}")]
        public async Task<IActionResult> LikeUser(int id, int recipientId)
        {
            // Check for valid token user
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            // Check if the like exists
            var like = await repo.GetLike(id, recipientId);

            if (like != null)
            {
                return BadRequest("You already liked this user.");
            }

            //  Check if recipient of Id exists
            if (await repo.GetUserAsync(recipientId) == null)
            {
                return NotFound();
            }

            like = new Like
            {
                LikerId = id,
                LikeeId = recipientId
            };

            repo.Add<Like>(like);

            if (await repo.SaveAllAsync())
            {
                return Ok();
            }
            return BadRequest("Failed to like user.");
        }

        // Get All Users
        // ===================================================================
        [HttpGet]
        public async Task<IActionResult> GetUsers([FromQuery] UserParams userParams)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var userFromRepo = await repo.GetUserAsync(currentUserId);
            userParams.UserId = currentUserId;

            // If a gender has been specified in UserParams
            if (string.IsNullOrEmpty(userParams.Gender))
            {
                userParams.Gender = userFromRepo.Gender.Equals("male") ? "female" : "male";
            }

            var users = await repo.GetUsersAsync(userParams);
            var usersToReturn = Mapper.Map<IEnumerable<UserForListDto>>(users);

            Response.AddPaginationHeaders(users.CurrentPage, users.PageSize, users.TotalCount, users.TotalPages);

            return Ok(usersToReturn);
        }


        // Get A User
        // ===================================================================
        [HttpGet("{id}", Name = "GetUser")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user = await repo.GetUserAsync(id);

            var userToReturn = Mapper.Map<UserForDetailedDto>(user);

            return Ok(userToReturn);

        }

        // Update the currnetly logged in user
        // ===================================================================
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserForUpdateDto userForUpdate)
        {
            // Check for valid token user
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var userFromRepo = await repo.GetUserAsync(id);

            Mapper.Map(userForUpdate, userFromRepo);

            if (await repo.SaveAllAsync())
            {
                return NoContent();
            }
            else
            {
                throw new Exception($"Updating user with {id} failed on save.");
            }
        }


    }
}